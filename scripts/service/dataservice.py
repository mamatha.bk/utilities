from flask import request, Blueprint
from scripts.handler.utilities import Utilities

data = Blueprint("Data", __name__)

utility_obj = Utilities()
@data.route('/', methods=['POST'])
def postjsonhandler():
    content = request.get_json()
    utility_obj.data_parsing(content)
    return str(content)
print("Select action to be performed")
print("1.Find\n 2.Update\n 3.Delete\n 4.Find_one")
option = int(input())
if option == 1:
    utility_obj.data_finding()
if option == 2:
    utility_obj.data_updating()
if option == 3:
    utility_obj.data_deleting()
if option == 4:
    utility_obj.data_finding_one()


