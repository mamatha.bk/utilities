from pymongo import MongoClient

client = MongoClient('localhost', 27017)
db = client["mamatha"]
Collection = db["user_details_2"]


class Utilities:
    @staticmethod
    def data_parsing(data):
        if isinstance(data, list):
            Collection.insert_many(data)
            print("Inserted")
        else:
            Collection.insert_one(data)
            print("Inserted")

    @staticmethod
    def data_finding():
        for data1 in Collection.find({}, {"gender": 1}):
            print(data1)

    @staticmethod
    def data_finding_one():
        one = Collection.find_one()
        print(one)

    @staticmethod
    def data_updating():
        myquery = {"gender": "Male"}
        newvalues = {"$set": {"gender": "male"}}
        Collection.update_many(myquery, newvalues)
        for data in Collection.find({}, {"gender": 1}):
            print(data)

    @staticmethod
    def data_deleting():
        myquery = {"first_name": "Vassili"}

        Collection.delete_one(myquery)
        for data in Collection.find({}, {"first_name": 1}):
            print(data)
